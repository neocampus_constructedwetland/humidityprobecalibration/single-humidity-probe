/* ******************************************************************************************** */
/* Global functions for the project.                                                            */
/* ******************************************************************************************** */
// Author: Jean-Louis Druilhe (jean-louis.druilhe@univ-tlse3.fr)

#ifndef FUNCTIONS_H_
#define FUNCTIONS_H_        1
/*----------------------------------------------------------------------------------*/
/*         Here the libraries call with all classes and their methods               */
/* [WARNING]: All instantiate objects in this module allow the usage of all methods */
/* and attributes declared in these libraries on the condition that they are called */
/* here.                                                                            */
/*----------------------------------------------------------------------------------*/
#include      <Wire.h>              // I2C library
#include      <OneWire.h>
#include      <DallasTemperature.h>
#include      <LiquidCrystal_I2C.h>
#include      <EEPROM.h>
//#include      <Adafruit_ADS1X15.h>
#include      <ADS1X15.h>
#include      <string.h>
#include      <stdio.h>
#include      <stdint.h>
#include      <stdlib.h>            /* atof() function */
#include      <avr/io.h>            /* sprintf and printf */
#include      <avr/interrupt.h>     /* interrupt vectors */
#include      <avr/pgmspace.h>
#include      <Arduino.h>

/*----------------------------------------------------------------------------------*/
/*                         C++ Preprocessor directives                              */
/* [NOTICE]: Shared preprocessor directives which are used by the main file or in   */
/* each module where they are declared. As modules are called by the main file, the */
/* directive is active in the main file and in the module himself.                  */
/* Each module needs to activate or inhibit directive in the header file obviously. */
/*----------------------------------------------------------------------------------*/
/*------------------------------------------------------------------------------------------------*/
/*                 Local directives to check some states when it is necessary                     */
/*------------------------------------------------------------------------------------------------*/
#define       messagesON
//#define       ADS115Connected

/*------------------------------------------------------------------------------------------------*/
/*                                       SYSTEM CONSTANTS                                         */
/*------------------------------------------------------------------------------------------------*/
//#define       LF                        0x0A      // '\n' this acronym is already used by the core
//#define       CR                        0x0D      // '\r' this acronym is already used by the core
#define       Space                     0x20
#define       Null                      0         // '\0'

/*------------------------------------------------------------------------------------------------*/
/*          Directives to inform timer functions to activate or deactivate UART resources         */
/*------------------------------------------------------------------------------------------------*/
#define       Timer0_ON                 0           // used as a position
#define       Timer1_ON                 1
#define       Timer2_ON                 2
#define       Timer3_ON                 3
#define       Timer4_ON                 4
#define       Timer5_ON                 5
#define       DS18B20Temp               11
#define       DelayTime2s               2000
#define       DelayTime5s               5000
#define       DelayTime10s              10000
#define       DelayTime20s              20000
#define       Timer1Length              5
#define       Timer2Length              0.1
#define       Timer3Length              200
#define       Timer4Length              100
#define       Timer5Length              50
// OneWire commands
#define         STARTConvert            0x44          // Tells device to take a temperature reading and put it on the scratchpad
#define         COPYSCRATCH             0x48          // Copy EEPROM
#define         READSCRATCHPAD          0xBE          // Read EEPROM
#define         WRITESCRATCHPAD         0x4E          // Write to EEPROM
#define         RECALLSCRATCH           0xB8          // Reload from last known
#define         READPOWERSUPPLY         0xB4          // Determine if device needs parasite power
#define         ALARMSEARCH             0xEC          // Query bus for devices with an alarm condition
#define         SEARCHROM               0xF0          // to identify the ROM codes of all devices on the same bus
#define         READROM                 0x33          // this command can only be used when one device is connected
#define         SKIPROM                 0xCC          // to address all devices without sending any ROM code
#define         DS18S20MODEL            0x10
#define         DS18B20MODEL            0x28
#define         DS1822MODEL             0x22
// Scratchpad locations
#define         TEMP_LSB                0
#define         TEMP_MSB                1
#define         HIGH_ALARM_TEMP         2
#define         LOW_ALARM_TEMP          3
#define         CONFIGURATION           4
#define         INTERNAL_BYTE           5
#define         COUNT_REMAIN            6
#define         COUNT_PER_C             7
#define         SCRATCHPAD_CRC          8
// Device resolution and accuracy
#define         TEMP_9_BIT              0x1F          //  9 bit
#define         TEMP_10_BIT             0x3F          // 10 bit
#define         TEMP_11_BIT             0x5F          // 11 bit
#define         TEMP_12_BIT             0x7F          // 12 bit
#define         DefaultSlope            -0.0078       // slope relation
#define         Default_y_intercept     185
#define         Channel_ADC             1
#define         VDD_nominal             3.3
#define         gain1_024               1.024
#define         gain2_048               2.048
#define         gain4_096               4.096
#define         ADS1115FullScale        32767
#define         Sign_Mask               0x80000000    // for float numbers defined with 4 bytes
#define         AddEepromProbe1         0x10
#define         AddEepromProbe3         0x40
#define         AddEepromProbe4         0x70
#define         ADS1X15_MODE_SLOWEST    0x00
#define         ADS1X15_MODE_MEDIUM     0x04
#define         ADS1X15_MODE_FASTEST    0x07
#define         ADS1X15PGA_6_144V       0x00           //  0  =  +- 6.144V  default
#define         ADS1X15PGA_4_096V       0x01           //  1  =  +- 4.096V
#define         ADS1X15PGA_2_048V       0x02           //  2  =  +- 2.048V
#define         ADS1X15PGA_1_024V       0x04           //  4  =  +- 1.024V
#define         ADS1X15PGA_0_512V       0x08           //  8  =  +- 0.512V
#define         ADS1X15PGA_0_256V       0x10           //  16 =  +- 0.256V

/*------------------------------------------------------------------------------------------------*/
/*               Predefined data types (Data Types and Predefined Structures)                     */
/* Definition types using keyword typedef and often in usage with typedef enum or typedef struct. */
/*------------------------------------------------------------------------------------------------*/
typedef enum Probes : uint8_t {
  Probe1 = 1,
  Probe2,
  Probe3,
  Probe4
} Probe_t;

typedef struct TemperaturesCF {
  float MeasTempC;
  float MeasTempF;
} TemperaturesCF_t;

typedef struct HumidityRH_ADC {
  uint16_t DigitalFromADC;
  float Potential;
  float Humidity;
} HumidityRH_ADC_t;

typedef union flottant {
  float float_value;
  uint8_t byte_value[4];    // little-endian representation
} MyFloat_t;

typedef struct LinearCoeffs {
  float y_intercept_corrected;
  float Slope;
} LinearCoeffs_t;



/*------------------------------------------------------------------------------------------------*/
/*                          Function prototypes or functions interface                            */
/*------------------------------------------------------------------------------------------------*/
void Init_Timers(uint8_t, uint8_t, uint16_t, uint8_t, uint16_t, uint16_t, uint16_t);
void initI2C_Devices(void);
void initLCD(void);
void initTempSensor(void);
TemperaturesCF_t MeasureTemp(void);
void AfficheAdresseCapteur(uint8_t);
boolean DelayTimeForTimer1(uint16_t);
void ADCStartUp(uint8_t, Probe_t);
HumidityRH_ADC_t MeasureHumidity(void);
void ConvFloatToString(float, uint8_t, uint8_t, char *);
uint8_t ConvertUint32ToASCIIChar(char *, uint32_t);
void DisplayMeasures(TemperaturesCF_t, HumidityRH_ADC_t);
uint8_t ConvertFloatToStringWithSign(float, uint8_t, char *);
uint8_t ConvertUint32ToDecimalCharAsciiArray(char *, uint8_t, uint32_t);
char *FillMyAnswerArray(void);
void CalibrateHumidityProbe(void);
void DividerTextForFunctions(uint8_t, char);
float DisplayADCcounting_potential(uint16_t);
void DisplayEepromContents(void);
void DisplayCalculusForSlope(void);






#endif /* FONCTIONS_H_ */
/* ######################################################################################################## */
// END of file
