// NOTE: This template allows to get a global framework to write C++ code with the main structures
// to be informed on each module which entails 2 files with the options.cpp and .h (header file).
// A module is make of two files, one called header file with option .h
// and the other with option .cpp or .c which is the body which contains alls the detail of functions.
// Some line comments will explain which code type is awaited.
// 
// Single Board computer used for this project is MEGA AVR
/* ############################################################################################################## */
// class String : https://www.arduino.cc/en/Reference/StringObject
/* ############################################################################################################## */

/*----------------------------------------------------------------------------------*/
/*                 Functions call and used only in the main module                  */
/*----------------------------------------------------------------------------------*/
#include      "Functions.h"
/*----------------------------------------------------------------------------------*/
/*            Common variables shared by the functions of this module               */
/* [WARNING]: By default you should prefer to declare local variables implemented   */
/* in modules when there is enough memory space to avoid shared variables.          */
/*----------------------------------------------------------------------------------*/
uint8_t                     Ctrl_Flags;
TemperaturesCF_t            MeasureTemperature;
boolean                     MyTest1;
boolean                     DelayTime;
HumidityRH_ADC_t            MyHumMeas;
int                         InComingByte;
char                        *cmd;
String                      Command;
boolean                     computer_msg_complete;
char                        computerdata[60];
uint8_t                     computer_bytes_received;

/*----------------------------------------------------------------------------------*/
/*    External variables encountered in other modules and used in the main file     */
/*----------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------*/
/*                                Class instances                                   */
/* [WARNING]: Objects declared here have to target a constructor of a library       */
/* called by the main program. The call of the constructor method imply here that   */
/* the object is common to all modules.                                             */
/* Consequently, instances have not the vocation to be declared in the main         */
/* program because we can not delete module without suppressed other declarations   */
/* in the main program.                                                             */
/*----------------------------------------------------------------------------------*/


/*----------------------------------------------------------------------------------*/
/*                          SBC and devices initialization                          */
/*----------------------------------------------------------------------------------*/
void setup() {
  Serial.begin(115200, SERIAL_8N1);
  while (!Serial);
  cli();                                        // Deactivate the interrupt jumps
  Ctrl_Flags = ((1<<Timer1_ON)|(1<<Timer3_ON));
  Init_Timers(Ctrl_Flags, 250, 10000, 200, 50000, 25000, 3125);     // Timer0 .... Timer5, avoid : Timer5_ON and Timer0_ON
  sei();
  initI2C_Devices();                            // Wire.begin() I2C bus initialization
  initLCD();
  initTempSensor();
  AfficheAdresseCapteur(DS18B20Temp);
  DisplayEepromContents();
  MyTest1 = false;
  DelayTime = false;
  computer_bytes_received = 0;              // characters received from terminal
  Command.reserve(100);
  Command = "start";
  computer_msg_complete = true;
  memset(computerdata, Null, sizeof(computerdata));
  Command.toCharArray(computerdata, Command.length() + 1);
}
/*----------------------------------------------------------------------------------*/
/*                                     Main loop                                    */
/*----------------------------------------------------------------------------------*/
void loop() {
  while (Serial.available() > 0) {                                  // While there is serial data from the computer
    InComingByte = Serial.read();                                   // read an int type from terminal (virtual int read(void);)
    if (InComingByte == '\n' || InComingByte == '\r') {             // if a newline character arrives, we assume a complete command has been received
      computerdata[computer_bytes_received] = '\0';
      computer_msg_complete = true;
      computer_bytes_received = 0;
    } else computerdata[computer_bytes_received++] = InComingByte;  // command not complete yet, so just add the byte to data array
  }
  
  if (computer_msg_complete == true) {
    cmd = computerdata;
    Command = String(cmd);
    Serial.print(F("[Console] Command from terminal: "));           // echo to the serial console
    Serial.println(cmd);
    if (Command.startsWith("humidity")) MyHumMeas = MeasureHumidity();
    if (Command.startsWith("temperature")) MeasureTemperature = MeasureTemp();
    if (Command.startsWith("cal")) CalibrateHumidityProbe();
    if (Command.startsWith("start")) DelayTime = true;
    if (Command.startsWith("stop")) DelayTime = false;
    if (Command.startsWith("slope")) DisplayCalculusForSlope();
    
    computer_msg_complete = false;
  }
  
  if (MyTest1 == true && DelayTime == true) {                        // delay time elapsed
    Serial.print(F("Temperature measured: \t\t"));
    MeasureTemperature = MeasureTemp();
    Serial.print(MeasureTemperature.MeasTempC);
    Serial.print(" °C");
    Serial.print(F("\tor\t"));
    Serial.print(MeasureTemperature.MeasTempF);
    Serial.println(" °F");
    MyHumMeas = MeasureHumidity();
    DisplayMeasures(MeasureTemperature, MyHumMeas);
    MyTest1 = false;
  }
  MyTest1 = DelayTimeForTimer1(DelayTime5s);
}




/* ######################################################################################################## */
// END of file
