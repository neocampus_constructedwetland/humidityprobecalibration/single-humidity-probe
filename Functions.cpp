// Author: Jean-Louis Druilhe (jean-louis.druilhe@univ-tlse3.fr)
// NOTE: 
//
// Probes which can be connected to the multiparameter probe according the communication bus used:
// I2C => Dissoled Oxygen (DO), pH (pH), Oydo Reduction Potential (ORP), Conductivity (EC), Luminosity (Lux), Platinum Resistance Thermometer (RTD) 
// OneWire => DS18B20 (Temp)

/*----------------------------------------------------------------------------------*/
/*                 Functions call to be used here in this module                    */
/*----------------------------------------------------------------------------------*/
#include      "Functions.h"

/*----------------------------------------------------------------------------------*/
/*            Common variables shared by the functions of this module               */
/* [WARNING]: By default you should prefer to declare local variables implemented   */
/* in functions when there is enough memory space to avoid shared variables.        */
/*----------------------------------------------------------------------------------*/
volatile uint16_t           cmpt1, cmpt2, cmpt3, cmpt4, cmpt5, cmpt_5ms;
volatile uint8_t            compt1, compt2, compt3, compt4, compt5, cmpt_100us;
int16_t                     scratch_16bitsFunc;
uint8_t                     scratch_8bitsFunc, numberOfDevices;
DeviceAddress               Device_AddressProbe1;               // array of 8 uint8_t (typedef uint8_t DeviceAddress[8];)
uint8_t                     OneWireAddress[8];
boolean                     DeviceFound;
float                       HumRH1, HumRH3, HumRH4;
HumidityRH_ADC_t            HumMeas1, HumMeas3, HumMeas4;
char                        TabAsciiFunction[20];
uint8_t                     BigEndianFormat[4];
boolean                     CheckI2cProbe1, CheckI2cProbe3, CheckI2cProbe4;
LinearCoeffs_t              CoeffProbe1, CoeffProbe3, CoeffProbe4;
uint16_t                    CountingForDryProbe1, CountingForDryProbe3, CountingForDryProbe4;
char                        ArrayForNewSlope[14];
char                        ArrayForNewYintercept[14];
char                        MyAnswer[64];

/*----------------------------------------------------------------------------------*/
/*    External variables encountered in other modules and used in this module       */
/* [WARNING]: You must call in this module, the library or other module which refer */
/* to these variables declared as external here.                                    */
/*----------------------------------------------------------------------------------*/

/*----------------------------------------------------------------------------------*/
/*                                Class instances                                   */
/* [WARNING]: Objects declared here have to target a constructor of a library       */
/* called by this module. The constructor which is a public methods with private    */
/* attributes. The declaration here is consistent, knowing that the call of         */
/* functions of a probe or electronic component imply two files more associated     */
/* to the project in which instances are representative  objects of the component   */
/* used.                                                                            */
/*----------------------------------------------------------------------------------*/
LiquidCrystal_I2C lcd(0x27, 20, 4);
OneWire Temp_Probe(DS18B20Temp);                // Setup a oneWire instance to communicate with any OneWire devices
DallasTemperature DS18B20Probe(&Temp_Probe);    // Pass our oneWire reference to Dallas Temperature
#define MyI2CDevice Wire
//Adafruit_ADS1115 MyADC_Probe1;                  // instantiate an object as Probe1 (constructors for Adafruit_ADS1015 and Adafruit_ADS1115)
//Adafruit_ADS1115 MyADC_Probe3;                  // instantiate an object as Probe3
//Adafruit_ADS1115 MyADC_Probe4;                  // instantiate an object as Probe4
ADS1115 MyADC_Probe1(0x48);                       // construct an ads1115 at address 0x48 (ADDR tied to GND)
ADS1115 MyADC_Probe3(0x4A);                       // construct an ads1115 at address 0x4A (ADDR tied to SDA)=> Adafruit: MyADC_Probe3.begin(0x4A);
ADS1115 MyADC_Probe4(0x4B);                       // construct an ads1115 at address 0x4B (ADDR tied to SCL)=> Adafruit: MyADC_Probe3.begin(0x4B);           

/*----------------------------------------------------------------------------------*/
/*                 INTERRUPTION FUNCTIONS (Interruption vectors)                    */
/*----------------------------------------------------------------------------------*/
/****************************************************************************************************/
/* Interruption programs for all the timers. Timer0 has not to be used because it is used by several*/
/* libraries.                                                                                       */
/****************************************************************************************************/
// Timer1 configured for time delay of 5 ms
ISR(TIMER1_COMPA_vect) {
  compt1++;                     // 8 bits
  cmpt1++;                      // 16 bits
}
// Timer2 configured for delay time of 100 µs
ISR(TIMER2_COMPA_vect) {
  compt2++;                     // 8 bits
  cmpt2++;                      // 16 bits
}
// Timer3 configured to define delay time of 200 ms
ISR(TIMER3_COMPA_vect) {
  compt3++;                     // 8 bits
  cmpt3++;                      // 16 bits

}
// Compteur 4 programmé pour des temporisations de 100 ms
ISR(TIMER4_COMPA_vect) {
  compt4++;                     // 8 bits
  cmpt4++;                      // 16 bits
}
// Compteur 5 programmé pour des temporisations longues de 50 ms
ISR(TIMER5_COMPA_vect) {
  compt5++;
  cmpt5++;
}

/*----------------------------------------------------------------------------------*/
/*                          FUNCTIONS of this module                                */
/*----------------------------------------------------------------------------------*/
/****************************************************************************************************/
/* Function to initiate the timers of MEGA card which embeds ATmega2560 microcontroller.            */
/* --- It is recommended to avoid timer0 which is used by several libraries.                        */
/* Timer0 (8 bits), delaytime: 1 ms, CTC mode, prescaler = 64 => OCR0A = 250                        */                                       
/* Timer1 (16 bits), delaytime: 5 ms, CTC mode, prescaler = 8 => OCR1A = 0x2710 (10000)             */
/* Timer2 (8 bits), delaytime: 100 µs, CTC mode, prescaler = 8 => OCR2A = 0xC8 (200)                */
/* Timer3 (16 bits), delaytime: 10 ms, CTC mode, prescaler = 64 => OCR3A = 0x09C4 (2500)            */
/* --- Timer3 will not have to be used while some others ressources use it. ---                     */
/* Timer4 (16 bits), delaytime: 100 ms, CTC mode, prescaler = 64 => OCR4A = 0x61A8 (25000)          */
/* --- Timer4 will not have to be used while some others ressources use it. ---                     */
/* Timer5 (16 bits), delaytime: 50 ms, CTC mode, prescaler = 256 => OCR5A = 0x0C35 (3125)           */
/* Timer0 => used by LiquidCrystal_I2C library. The Timer0 will not have to be configured.          */
/* Above all the clock of the timer0 will not have to be stopped and is applicable only for him.    */
/****************************************************************************************************/
void Init_Timers(uint8_t drapeauxTimers, uint8_t time_Timer0, uint16_t time_Timer1, uint8_t time_Timer2, uint16_t time_Timer3, uint16_t time_Timer4, uint16_t time_Timer5) {
  uint8_t test_drapeaux;
  test_drapeaux = (drapeauxTimers & (1 << Timer0_ON));      // The only one which has not to be modified (used by LiquidCrystal_I2C.h with I2C mode)
  if (test_drapeaux != 0) {
    /* compteur 0 */
    TCCR0A = 0;                                             // (By default) WGM00, WGM01, COM0B0, COM0B1, COM0A0, COM0A1 = 0
    TCCR0A |= (1 << WGM01);                                 // CTC (Mode 2) => Clear Timer on Compare match
    TCCR0B = 0;                                             // WGM02 = 0
    TCCR0B |= ((1 << CS00) | (1 << CS01));                  // division by 64 => 250 KHz
    OCR0A = time_Timer0;                                    // delaytime of 1 ms with decimal value of 250 (0xFA)
    TIMSK0 |= (1 << OCIE0A);                                // interruption vectors
    TCNT0 = 0;
  } else {
    //TCCR0B &= ~((1<<CS00)|(1<<CS01)|(1<<CS02));           // has not to be blocked because display uses this interruption
  }
  test_drapeaux = (drapeauxTimers & (1 << Timer1_ON));      // Timer1 (16 Bit) is configured for 5 ms
  if (test_drapeaux != 0) {
    /* compteur 1 => WGM10=0, WGM11=0, WGM12=1, WGM13=0 => CTC */
    TCCR1A = 0;                                             // WGM10 and WGM11 are 0
    TCCR1B = (1 << WGM12);                                  // Mode 4 Timer1
    TCCR1B |= (1 << CS11);                                  // prescaler = 8 et WGM13 = 0 => 2 MHz
    TIMSK1 |= (1 << OCIE1A);                                // allows interruption by matching: that is a type of interruption
    TCNT1 = 0;
    scratch_16bitsFunc = time_Timer1;
    OCR1AH = (uint8_t)(scratch_16bitsFunc >> 8);            // 0x2710 = 10000
    OCR1AL = (uint8_t)time_Timer1;                          // tempo = 10000 x 1/(16MHz/8) = 5 ms
  } else {
    TCCR1B &= ~((1 << CS10) | (1 << CS11) | (1 << CS12));   // Timer1 is blocked
  }
  test_drapeaux = (drapeauxTimers & (1 << Timer2_ON));      // Timer2 (8 Bit) is configured for 100 µs
  if (test_drapeaux != 0) {
    /* compteur 2 (configuration CTC) compteur 8 bits */
    TCCR2A |= (1 << WGM21);                                 // CTC (Mode 2)
    TCCR2B = 0;                                             // WGM02 = 0, CS20 = 0, CS21 = 0, CS22 = 0
    TCCR2B |= (1 << CS21);                                  // division by 8 => 2 MHz
    OCR2A = time_Timer2;                                    // delaytime of 100 µs => decimal value 200 (0xC8)
    TIMSK2 |= (1 << OCIE2A);                                // interruption vectors
    TCNT2 = 0;
  } else {
    TCCR2B &= ~((1 << CS20) | (1 << CS21) | (1 << CS22));   // Timer2 is blocked
  }
  test_drapeaux = (drapeauxTimers & (1 << Timer3_ON));      // Timer3 (16 Bit) is configured for 200 ms
  if (test_drapeaux != 0) {
    TCCR3B |= (1 << WGM32);                                 // WGMn2 is the only flag activated (mode 4 => CTC)
    TCCR3A = 0;
    TCCR3B |= ((1 << CS31) | (1 << CS30));                  // division by 64 => 250 Khz
    scratch_16bitsFunc = time_Timer3;                       // 50000 to be converted
    OCR3AH = (uint8_t)(scratch_16bitsFunc >> 8);            // 0x09C4 = 2500
    OCR3AL = (uint8_t)time_Timer3;
    TIMSK3 |= (1 << OCIE3A);                                // allows interruption by matching
    TCNT3 = 0;                                              // Initialization of the timer is not mandatory
  } else {
    //TCCR3B &= ~((1 << CS30) | (1 << CS31) | (1 << CS32)); // Blocking the counter which will be activated by other resources
  }
  test_drapeaux = (drapeauxTimers & (1 << Timer4_ON));      // Timer4 (16 Bit) is configured for 100 ms
  if (test_drapeaux != 0) {
    TCCR4B |= (1 << WGM42);                                 // WGMn2 must be the only flag activated (mode 4 => CTC)
    //TCCR4A = 0;
    TCCR4B |= ((1 << CS41) | (1 << CS40));                  // division by 64 => 250 Khz
    scratch_16bitsFunc = time_Timer4;                       // 25000 to be converted for 100 ms
    OCR4AH = (uint8_t)(scratch_16bitsFunc >> 8);            // 0x61A8 = 25000
    OCR4AL = (uint8_t)time_Timer4;
    TIMSK4 |= (1 << OCIE4A);                                // allows interruption by matching
    TCNT4 = 0;                                              // Initialization of the timer is not mandatory
  } else {
    //TCCR4B &= ~((1 << CS40) | (1 << CS41) | (1 << CS42)); // Blocking the counter which will be activated by other resources
  }
  test_drapeaux = (drapeauxTimers & (1 << Timer5_ON));      // Timer5 (16 Bit) is configured for 50 ms
  if (test_drapeaux != 0) {
    TCCR5B |= (1 << WGM52);                                 // WGMn2 must be the only flag activated (mode 4 => CTC)
    //TCCR5A = 0;
    TCCR5B |= (1 << CS52);                                  // division by 256 => 62,5 Khz
    scratch_16bitsFunc = time_Timer5;                       // 3125 to be converted for 50 ms
    OCR5AH = (uint8_t)(scratch_16bitsFunc >> 8);            // 0x0C35 = 3125
    OCR5AL = (uint8_t)time_Timer5;
    TIMSK5 |= (1 << OCIE5A);                                // allows interruption by matching
    TCNT5 = 0;                                              // Initialization of the timer is not mandatory
  } else {
    TCCR5B &= ~((1 << CS50) | (1 << CS51) | (1 << CS52));   // Blocking the counter which will be activated by other resources
  }
}
/****************************************************************************************************/
/* Function to initiate the I2C bus which is an object called Wire.                                 */
/* MyI2CDevice is declared using a compilation directive #define MyI2CDevice Wire in this module.   */
/****************************************************************************************************/
void initI2C_Devices() {
  uint8_t NbrDevice = 0;
  uint8_t k;  
  MyI2CDevice.begin();                                // enable I2C port
  Serial.println(F("---------------------------"));
  Serial.println(F("Starting  I2C scan..."));  
  for (k = 1; k < 128; k++) {
    MyI2CDevice.beginTransmission(k);                 // If true, endTransmission() sends a stop message after transmission, releasing the I2C bus
    if (MyI2CDevice.endTransmission() == 0) {         // if device is present
      NbrDevice++;                                    // number of device increased
      Serial.print(F("I2C used channel 0x"));
      if (k < 16) Serial.print("0");
      Serial.println(k, HEX);                         // hexadecimal address
      switch (k) {
        case 0x48:
          ADCStartUp(ADS1X15PGA_4_096V, Probe1);
          CheckI2cProbe1 = true;
          CheckI2cProbe3 = false;
          CheckI2cProbe4 = false;
          Serial.println(F("(\u2714 Probe1 found"));
          break;
        case 0x4A:
          ADCStartUp(ADS1X15PGA_4_096V, Probe3);
          CheckI2cProbe3 = true;
          CheckI2cProbe1 = false;
          CheckI2cProbe4 = false;
          Serial.println(F("\u2714 Probe3 found"));
          break;
        case 0x4B:
          ADCStartUp(ADS1X15PGA_4_096V, Probe4);
          CheckI2cProbe4 = true;
          CheckI2cProbe1 = false;
          CheckI2cProbe3 = false;
          Serial.println(F("(\u2714 Probe4 found"));
          break;
        default:
          break;
      }
    }
  }
  Serial.println(F("SCAN COMPLETE"));
  Serial.print(F("I2C devices encountered: "));
  Serial.println(NbrDevice, DEC);
  Serial.println(F("---------------------------"));
}
/****************************************************************************************************/
/* Function to initiate the alphanumeric LCD.                                                       */
/****************************************************************************************************/
void initLCD() {
  lcd.begin();
  lcd.setCursor(0, 0); 
  lcd.print("-- Humidity probe --");
  lcd.setCursor(0,1);
  lcd.print("Temperature (");
  lcd.write(0xDF);
  lcd.print("C)");
  lcd.setCursor(0,2);
  lcd.print("Humidity (%RH)");
}
/****************************************************************************************************/
/* Function to initiate the OneWire temperature sensor DS18B20.                                     */
/****************************************************************************************************/
void initTempSensor() {
  uint8_t k;
  uint8_t DS18B20Resolution;
  DS18B20Probe.begin();
  numberOfDevices = DS18B20Probe.getDeviceCount();       // Grab a count of devices on the wire
  Serial.print(F("Number of devices found: "));
  Serial.println(numberOfDevices, DEC);
  DeviceFound = DS18B20Probe.getAddress(Device_AddressProbe1, 0);
  lcd.setCursor(0,3);
  for (k = 0; k < 8; k++) {                             // convert decimal uint8_t (binary) value to HexASCII chars
    scratch_8bitsFunc = Device_AddressProbe1[7 - k];
    scratch_8bitsFunc >>= 4;
    lcd.print((char)scratch_8bitsFunc, HEX);            // convert uint8_t to char 0x01 => 0x31
    scratch_8bitsFunc = Device_AddressProbe1[7 - k];
    scratch_8bitsFunc &= 0x0F;
    lcd.print((char)scratch_8bitsFunc, HEX);
    //lcd.write(0x20);
  }
  Serial.println();
  DS18B20Resolution = DS18B20Probe.getResolution(&Device_AddressProbe1[0]);
  if (DS18B20Resolution != 12) DS18B20Probe.setResolution(TEMP_12_BIT);
  Serial.print(F("ADC resolution: "));
  Serial.println(12, DEC);
}
/****************************************************************************************************/
/* Function to get measure of temperature in Celsius.                                               */
/****************************************************************************************************/
TemperaturesCF_t MeasureTemp() {
  TemperaturesCF_t MeasuresFromDS18B20;
  boolean ConnectedProbe;
  ConnectedProbe = DS18B20Probe.requestTemperaturesByAddress(&Device_AddressProbe1[0]);
  if (ConnectedProbe == true) {
    MeasuresFromDS18B20.MeasTempC = DS18B20Probe.getTempC(&Device_AddressProbe1[0]);
    MeasuresFromDS18B20.MeasTempF = DS18B20Probe.getTempF(&Device_AddressProbe1[0]);
  }
  return MeasuresFromDS18B20;
}
/****************************************************************************************************/
/* Function which displays the content of the unidimensional array of the selected address probe.   */
/****************************************************************************************************/
void AfficheAdresseCapteur(uint8_t GpioUsed) {
  uint8_t *local_ptr;
  uint8_t HighNibble;                 // HighQuartet
  uint8_t k;
  local_ptr = &Device_AddressProbe1[0]; 
  Serial.print(F("Selected probe address at GPIO "));
  Serial.print(GpioUsed, DEC);
  Serial.print(F(":\t"));
  for (k = 0; k < 8; k++) {
    if (*local_ptr > 15) Serial.print(*(local_ptr++), HEX);
    else {
      Serial.print('0');
      Serial.print(*(local_ptr++), HEX);
    }
    Serial.print(' ');
  }
  Serial.println();
  DeviceFound = DS18B20Probe.getAddress(OneWireAddress, 0);
  local_ptr = &OneWireAddress[0];
  if (DeviceFound == true) {
    Serial.print(F("Lasered ROM code:\t\t\t"));
    for (k = 0; k < 8; k++) {
      scratch_8bitsFunc = *(local_ptr++);
      HighNibble = scratch_8bitsFunc;
      HighNibble &= 0xF0;
      HighNibble >>= 4;
      scratch_8bitsFunc &= 0x0F;
      if (HighNibble < 10) Serial.print(char(HighNibble + 0x30));
      else Serial.print(char(HighNibble + 55));
      if (scratch_8bitsFunc < 10) Serial.print(char(scratch_8bitsFunc + 0x30));
      else Serial.print(char(scratch_8bitsFunc + 55));
      Serial.print(char(0x20));
    }
  }
  Serial.println();
}
/****************************************************************************************************/
/* Delaytime using interruptions ot Timer1 with length of 5 ms.                                     */
/****************************************************************************************************/
boolean DelayTimeForTimer1(uint16_t localdelay) {
  uint16_t NbrCycle;
  boolean test = false;
  NbrCycle = (uint16_t)(localdelay / Timer1Length);
  if (cmpt1 > NbrCycle) {
    cmpt1 = 0;
    test = true;
    //Serial.println(NbrCycle, DEC);
  }
  return test;
}
/****************************************************************************************************/
/* identification of probes on I2C bus from which instance are declared.                            */
/* With 5 Volts power supply 5 V: ads1115.setGain(GAIN_ONE);  // 1x gain +/- 4.096V 1 bit = 2mV     */
/* With 3.3 Volts power supply: ads1115.setGain(GAIN_TWO);                                          */
/****************************************************************************************************/
void ADCStartUp(uint8_t ADC_Gain, Probe_t ProbeSelected) {      // adsGain_t define in Adafruit_ADS1X15.h
  switch (ProbeSelected) {
    case Probe1:
      MyADC_Probe1.begin();                   // (ADDR tied to GND)
      MyADC_Probe1.setGain(ADC_Gain);         // 2x gain +/- 2.048 Volts LSB = 62,5 µV          
      //MyADC_Probe1.setDataRate(ADS1X15_REG_CONFIG_MODE_SINGLE);           // Adafruit
      MyADC_Probe1.setDataRate(ADS1X15_MODE_MEDIUM);
      break;
    case Probe3:
      MyADC_Probe3.begin();                   // (ADDR tied to SDA)
      MyADC_Probe3.setGain(ADC_Gain);         // 2x gain +/- 2.048 Volts LSB = 62,5 µV
      //MyADC_Probe3.setDataRate(ADS1X15_REG_CONFIG_MODE_SINGLE);
      MyADC_Probe3.setDataRate(ADS1X15_MODE_MEDIUM);
      break;    
    case Probe4:
      MyADC_Probe4.begin();                   // (ADDR tied to SCL)
      MyADC_Probe4.setGain(ADC_Gain);         // 2x gain +/- 2.048 Volts LSB = 62,5 µV
      //MyADC_Probe4.setDataRate(ADS1X15_REG_CONFIG_MODE_SINGLE);
      MyADC_Probe4.setDataRate(ADS1X15_MODE_MEDIUM);
      break;    
    default:
      break;
  }
}
/****************************************************************************************************/
/* Function to get measures of humidity using ADC ADS1115.                                          */
/* linear equation: HR% = -0.0078 x ADCcounter + 185 without correction.                            */
/* dry condition: 21640 pulses, wet condition calculated with probe submerged in water: 8820 pulses */
/* First slope: 100 / (8820 - 21640) = -0.0078 (RegLin).                                            */
/* Corrected slope: 90 / (8820 - 21640) = -0.0070.                                                  */
/* METHOD:                                                                                          */
/* 1) We have to use the default linear equation (HR% = -0.0078 x ADCcounter + 185)                 */
/* 2) We measure the humidity with the probe exposed to dry air to get the humidity offset          */
/* 3) We notice for dry air the counter from ADC for a humidity of 0 %                              */
/* 4) We reduce b coefficient with humidity offset value: y_intercept - First_y_corrected           */
/* 5) From default slope, we calculate the counter value for a humidity of 100 %                    */
/*    slope = (Delta Y/Delta X) = (100 % - 0 %) / (ADC_x1 - 21640) = -0.0078                        */
/*    ADC_x1 = (0.0078 x 21640 - 100) / 0.0078 = 8820                                               */
/* 6) We calculate the new slope considering that for 8820 count pulses we have a hmidity of 90 %   */
/*    and not 100 % as the calculus above.                                                          */
/* 7) New Slope => Slope1 = 90 / (8820 - 21640) = -0.007                                            */
/* 8) We apply the new slope to the default equation and we read the new humidity rate for dry air  */
/* 9) We reduce the b coefficient adding the humidity value read just above to the first humidity   */
/*    offset read above step 2 => (First_y_corrected + Second_y_corrected)                          */
/****************************************************************************************************/
HumidityRH_ADC_t MeasureHumidity() {
  char *LclPtr;
  uint8_t k;
  float Slope;
  
  if (CheckI2cProbe1 == true) {
    //HumMeas1.DigitalFromADC = MyADC_Probe1.readADC_SingleEnded(Channel_ADC);                              // send back an uint16_t
    HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);
    HumMeas1.Potential = ((float)HumMeas1.DigitalFromADC * gain2_048) / (float)ADS1115FullScale;
    EEPROM.get(AddEepromProbe1, CoeffProbe1);
    Slope = CoeffProbe1.Slope;
    if (Slope > -0.004 && Slope < -0.009) {
      HumMeas1.Humidity = (CoeffProbe1.Slope * (float)HumMeas1.DigitalFromADC) + (float)(CoeffProbe1.y_intercept_corrected);
    } else {
      HumMeas1.Humidity = (DefaultSlope * (float)HumMeas1.DigitalFromADC) + (float)Default_y_intercept;   // y = a.x + b with a < 0 and b = 185 %RH
    }
    Serial.print(F("\u2714 Differential voltage measured from ADS1115: "));
    Serial.print(HumMeas1.Potential);
    if (HumMeas1.Potential >= 2.0) Serial.println(F(" volts"));
    else Serial.println(F(" volt"));
    Serial.print(F("\u2714 Relative humidity: "));
    if (HumMeas1.Humidity < 10.0) {
      ConvFloatToString(HumMeas1.Humidity, 4, 2, &TabAsciiFunction[0]);               // char TabAsciiFunction[20];
      LclPtr = &TabAsciiFunction[0];
      for (k = 0; k < 4; k++) Serial.print(*(LclPtr++));
    } else {
      ConvFloatToString(HumMeas1.Humidity, 5, 2, &TabAsciiFunction[0]);
      LclPtr = &TabAsciiFunction[0];
      for (k = 0; k < 5; k++) Serial.print(*(LclPtr++));
    }
    Serial.println(F(" %RH\n"));
    return HumMeas1;
  }
  
  if (CheckI2cProbe3 == true) {
    HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);                                              // send back an uint16_t
    HumMeas3.Potential = ((float)HumMeas3.DigitalFromADC * gain2_048) / (float)ADS1115FullScale;
    EEPROM.get(AddEepromProbe3, CoeffProbe3);
    Slope = CoeffProbe3.Slope;
    if (Slope < -0.004 && Slope > -0.009) {
      HumMeas3.Humidity = (CoeffProbe3.Slope * (float)HumMeas3.DigitalFromADC) + (float)(CoeffProbe3.y_intercept_corrected);
      Serial.println(F("Corrected measures"));
    } else {
      HumMeas3.Humidity = (DefaultSlope * (float)HumMeas3.DigitalFromADC) + (float)Default_y_intercept;       // y = a.x + b with a < 0 and b = 185 %RH
    }
    Serial.print(F("\u2714 Differential voltage measured from ADS1115: "));
    Serial.print(HumMeas3.Potential);
    if (HumMeas3.Potential >= 2.0) Serial.println(F(" volts"));
    else Serial.println(F(" volt"));
    Serial.print(F("\u2714 Relative humidity: "));
    if (HumMeas3.Humidity < 10.0) {
      ConvFloatToString(HumMeas3.Humidity, 4, 2, &TabAsciiFunction[0]);               // char TabAsciiFunction[20];
      LclPtr = &TabAsciiFunction[0];
      for (k = 0; k < 4; k++) Serial.print(*(LclPtr++));
    } else {
      ConvFloatToString(HumMeas3.Humidity, 5, 2, &TabAsciiFunction[0]);
      LclPtr = &TabAsciiFunction[0];
      for (k = 0; k < 5; k++) Serial.print(*(LclPtr++));
    }
    Serial.println(F(" %RH\n"));
    return HumMeas3;
  }

  if (CheckI2cProbe4 ==true) {
    HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);                              // send back an uint16_t
    HumMeas4.Potential = ((float)HumMeas4.DigitalFromADC * gain2_048) / (float)ADS1115FullScale;
    EEPROM.get(AddEepromProbe4, CoeffProbe4);
    Slope = CoeffProbe4.Slope;
    if (Slope > -0.004 && Slope < -0.009) {
      HumMeas4.Humidity = (CoeffProbe4.Slope * (float)HumMeas4.DigitalFromADC) + (float)(CoeffProbe4.y_intercept_corrected);
    } else {
      HumMeas4.Humidity = (DefaultSlope * (float)HumMeas4.DigitalFromADC) + (float)Default_y_intercept;   // y = a.x + b with a < 0 and b = 185 %RH
    }
    Serial.print(F("\u2714 Differential voltage measured from ADS1115: "));
    Serial.print(HumMeas4.Potential);
    if (HumMeas4.Potential >= 2.0) Serial.println(F(" volts"));
    else Serial.println(F(" volt"));
    Serial.print(F("\u2714 Relative humidity: "));
    if (HumMeas4.Humidity < 10.0) {
      ConvFloatToString(HumMeas4.Humidity, 4, 2, &TabAsciiFunction[0]);               // char TabAsciiFunction[20];
      LclPtr = &TabAsciiFunction[0];
      for (k = 0; k < 4; k++) Serial.print(*(LclPtr++));
    } else {
      ConvFloatToString(HumMeas4.Humidity, 5, 2, &TabAsciiFunction[0]);
      LclPtr = &TabAsciiFunction[0];
      for (k = 0; k < 5; k++) Serial.print(*(LclPtr++));
    }
    Serial.println(F(" %RH\n"));
    return HumMeas4;
  }
}
/****************************************************************************************************/
/* Function to replace the equivalent method dtostrf.                                               */
/* char *dtostrf(double val, signed char width, unsigned char prec, char *s)                        */
/* the aim is to fill the char array identified with the litteral values of double or float number. */
/* signed char width : number of alphanumeric values, comma included.                               */
/* unsigned char prec : precision of the float or number of digits just after the comma.            */
/* essential link to get an example for conversion: https://www.esp8266.com/viewtopic.php?t=3592    */
/****************************************************************************************************/
void ConvFloatToString(float ConvertFloat, uint8_t Width, uint8_t NbrDecimals, char *DestArray) {
  uint8_t k;
  MyFloat_t LocalFloat;
  LocalFloat.float_value = ConvertFloat;
  uint32_t IEEE754Representation = 0;
  uint32_t IntegerResult;
  char Scratch_tab[10];                                                               // to convert uint32_t in ASCII format
  uint8_t NbrChar;
  uint8_t CommaPosition;
  for (k = 4; k > 0; k--) BigEndianFormat[k - 1] = LocalFloat.byte_value[4 - k];      // big-endian representation
  for (k = 4; k > 0; k--) {
    IEEE754Representation |= LocalFloat.byte_value[k - 1];
    if (k != 1) IEEE754Representation <<= 8;
  }
  if (IEEE754Representation & Sign_Mask) {                                            // #define Sign_Mask 0x80000000
    *(DestArray++) = '-';
    LocalFloat.float_value *= -1.0;
  }
  switch (NbrDecimals) {
    case 0:
      IntegerResult = (uint32_t)LocalFloat.float_value;
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      for (k = 0; k < NbrChar; k++) *(DestArray++) = Scratch_tab[k];
      *DestArray = Null;
      break;
    case 1:
      IntegerResult = (uint32_t)(LocalFloat.float_value * 10.0);
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      CommaPosition = NbrChar - 1;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = Scratch_tab[k];
      *(DestArray++) = '.';
      *(DestArray++) = Scratch_tab[CommaPosition];
      *DestArray = Null;
      break;
    case 2:
      IntegerResult = (uint32_t)(LocalFloat.float_value * 100.0);
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      CommaPosition = NbrChar - 2;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = Scratch_tab[k];
      *(DestArray++) = '.';
      for (k = 0; k < 2; k++) *(DestArray++) = Scratch_tab[CommaPosition + k];
      *DestArray = Null;
      break;
    case 3:
      IntegerResult = (uint32_t)(LocalFloat.float_value * 1000.0);
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      CommaPosition = NbrChar - 3;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = Scratch_tab[k];
      *(DestArray++) = '.';
      for (k = 0; k < 3; k++) *(DestArray++) = Scratch_tab[CommaPosition + k];
      *DestArray = Null;
      break;
    case 4:
      IntegerResult = (uint32_t)(LocalFloat.float_value * 10000.0);
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      CommaPosition = NbrChar - 4;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = Scratch_tab[k];
      *(DestArray++) = '.';
      for (k = 0; k < 4; k++) *(DestArray++) = Scratch_tab[CommaPosition + k];
      *DestArray = Null;
      break;
    case 5:
      IntegerResult = (uint32_t)(LocalFloat.float_value * 100000.0);
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      CommaPosition = NbrChar - 5;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = Scratch_tab[k];
      *(DestArray++) = '.';
      for (k = 0; k < 5; k++) *(DestArray++) = Scratch_tab[CommaPosition + k];
      *DestArray = Null;
      break;      
    default:
      IntegerResult = (uint32_t)(LocalFloat.float_value * 100.0);
      NbrChar = ConvertUint32ToASCIIChar(&Scratch_tab[0], IntegerResult);
      CommaPosition = NbrChar - 2;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = Scratch_tab[k];
      *(DestArray++) = '.';
      for (k = 0; k < 2; k++) *(DestArray++) = Scratch_tab[CommaPosition + k];
      *DestArray = Null;
      break;   
  }
}
/**********************************************************************************************************************************/
/* Function to convert an uint32_t (hexadecimal form) into a decimal ASCII representation to replace sprintf for long integer.    */
/* The conversion is made from low significant bit to high significant bit and the number of significant digit is returned.       */                
/**********************************************************************************************************************************/
uint8_t ConvertUint32ToASCIIChar(char *ptrTAB, uint32_t valToConvert) {   // 10 possible characters from 0 to 4,294,967,295
  char *ptrINIT;
  uint8_t m, k;
  uint8_t indice;
  uint8_t result_modulo;
  uint32_t result_division;
  ptrINIT = ptrTAB;
  for (m = 0; m < 10; m++) *(ptrTAB++) = Null;      // initialisation of the array
  indice = 9;                                       // the low significant digit in the char array (unity)
  ptrTAB = ptrINIT;
  ptrTAB += indice * sizeof(char);                  // to fix the low digit
  do {
    result_modulo = (uint8_t)(valToConvert % 0x0A);
    *(ptrTAB--) = (char)(result_modulo + 0x30);     // ASCII char to display
    indice--;
    result_division = (valToConvert - result_modulo) / 0x0A;
    valToConvert = result_division;                 // new value for which we have to define the modulo
  } while (result_division > 15);                   // if result is between 0 and 15, we can identify all characters to be displayed
  if (result_division >= 1 && result_division <= 9) *ptrTAB = (char)(0x30 + result_division);
  if (result_division >= 10 && result_division <= 15) {
    scratch_8bitsFunc = result_division - 0x0A;
    *(ptrTAB--) = (char)(0x30 + scratch_8bitsFunc);
    indice--;
    *ptrTAB = (char)0x31;
  }
  ptrTAB = ptrINIT;                                 // first position in the array
  ptrINIT += indice * sizeof(char);                 // to retrieve the last position of the most significant digit
  for (k = 0; k < 10 - indice; k++) *(ptrTAB++) = *(ptrINIT++);   // to retrieve an array starting with the fisrt position [0]
  for (k = 10 - indice; k < 10; k++) *(ptrTAB++) = 0x20;          // 10 is the dimension of the array Scratch_tab[]
  return 10 - indice;
}
/****************************************************************************************************/
/* Function to display measures temperature and humidity only.                                      */
/****************************************************************************************************/
void DisplayMeasures(TemperaturesCF_t MyTemp, HumidityRH_ADC_t MyHumidity) {
  char *lclptr;
  uint8_t NbrChar;
  uint8_t k;
  lcd.clear();
  lcd.setCursor(0, 0); 
  lcd.print("Temp...: ");
  memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
  ConvertFloatToStringWithSign(MyTemp.MeasTempC, 2, &TabAsciiFunction[0]);
  lclptr = &TabAsciiFunction[0];
  do {
    lcd.print(*(lclptr++));
  } while (*lclptr != Null);
  lcd.write(0x20);
  lcd.write(0xDF);
  lcd.print("C");
  lcd.setCursor(0,1);
  lcd.print("Humidity: ");
  memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
  ConvertFloatToStringWithSign(MyHumidity.Humidity, 2, &TabAsciiFunction[0]);
  lclptr = &TabAsciiFunction[0];
  do {
    lcd.print(*(lclptr++));
  } while (*lclptr != Null);
  lcd.print("%");
  lcd.setCursor(0,2);
  lcd.print("ADC chnl1: ");
  memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
  ConvertFloatToStringWithSign(MyHumidity.Potential, 3, &TabAsciiFunction[0]);
  lclptr = &TabAsciiFunction[0];
  do {
    lcd.print(*(lclptr++));
  } while (*lclptr != Null);
  lcd.print(" V");
  lcd.setCursor(0,3);
  lcd.print("Counter: ");
  memset(TabAsciiFunction, Null, sizeof(TabAsciiFunction));
  NbrChar = ConvertUint32ToASCIIChar(&TabAsciiFunction[0], (uint32_t)MyHumidity.DigitalFromADC);
  lclptr = &TabAsciiFunction[0];
  for (k = 0; k < NbrChar; k++) lcd.print(*(lclptr++));
}
/****************************************************************************************************/
/* Function to replace the equivalent method dtostrf.                                               */
/* char *dtostrf(double val, signed char width, unsigned char prec, char *s)                        */
/* The aim is to fill the char array identified with the values of a double or a float number.      */
/* Serial object methods like print and println do not allow display the correct content of a real. */
/* The signed char width : number of alphanumeric values, comma included ('.').                     */
/* Unsigned char prec : precision of the float or number of digits just after the comma.            */
/* essential link to get an example for conversion: https://www.esp8266.com/viewtopic.php?t=3592    */
/****************************************************************************************************/
uint8_t ConvertFloatToStringWithSign(float ConvertFloat, uint8_t NbrDecimals, char *DestArray) {
  uint8_t k;
  uint8_t NbrAsciiCharOfString = 0;         // string width : number of ASCII digit returned, comma included.
  uint32_t IntegerResult;
  char ScratchArray[15];                    // to convert positive uint32_t in ASCII format
  uint8_t NbrChar;
  uint8_t CommaPosition;

  if (ConvertFloat < 0.00f) {
    *(DestArray++) = '-';
    ConvertFloat *= -1.0f;
    NbrAsciiCharOfString++;
    //Serial.println(F("Yes the value is negative"));
  }
  memset(ScratchArray, Null, sizeof(ScratchArray));

  switch (NbrDecimals) {
    case 0:
      IntegerResult = (uint32_t)ConvertFloat;
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
      *DestArray = Null;
      NbrAsciiCharOfString += NbrChar;
      break;
    case 1:
      IntegerResult = (uint32_t)(ConvertFloat * 10.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);  // NbrChar = 3
      if (IntegerResult < 10) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (1 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (1 + 2);
      } else {
        CommaPosition = NbrChar - 1;                                                                          // CommaPosition = 2
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        *(DestArray++) = ScratchArray[CommaPosition];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 2:
      IntegerResult = (uint32_t)(ConvertFloat * 100.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 100) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (2 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (2 + 2);
      } else {
        CommaPosition = NbrChar - 2;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 2; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 3:
      IntegerResult = (uint32_t)(ConvertFloat * 1000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 1000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (3 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (3 + 2);
      } else {
        CommaPosition = NbrChar - 3;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 3; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 4:
      IntegerResult = (uint32_t)(ConvertFloat * 10000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 10000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (4 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (4 + 2);
      } else {
        CommaPosition = NbrChar - 4;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 4; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 5:
      IntegerResult = (uint32_t)(ConvertFloat * 100000.0f);
      //Serial.print(F("IntegerResult: ")); Serial.println(IntegerResult, DEC);             // OK
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      // Serial.print(F("NbrChar: ")); Serial.println(NbrChar, DEC);                        // OK
      if (IntegerResult < 100000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (5 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (5 + 2);
      } else {
        CommaPosition = NbrChar - 5;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 5; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 6:
      IntegerResult = (uint32_t)(ConvertFloat * 1000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 1000000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (6 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (6 + 2);
      } else {
        CommaPosition = NbrChar - 6;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 6; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 7:
      IntegerResult = (uint32_t)(ConvertFloat * 10000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 10000000) {                     // float lower than 1
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (7 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (7 + 2);
      } else {
        CommaPosition = NbrChar - 7;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 7; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 8:
      IntegerResult = (uint32_t)(ConvertFloat * 100000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 100000000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (8 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (8 + 2);
      } else {
        CommaPosition = NbrChar - 8;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 8; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    case 9:
      IntegerResult = (uint32_t)(ConvertFloat * 1000000000.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      if (IntegerResult < 1000000000) {
        *(DestArray++) = (char)0x30;
        *(DestArray++) = '.';
        for (k = 0; k < (9 - NbrChar); k++) *(DestArray++) = (char)0x30;
        for (k = 0; k < NbrChar; k++) *(DestArray++) = ScratchArray[k];
        NbrAsciiCharOfString += (9 + 2);
      } else {
        CommaPosition = NbrChar - 9;
        for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
        *(DestArray++) = '.';
        for (k = 0; k < 9; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
        *DestArray = Null;
        NbrAsciiCharOfString += (NbrChar + 1);
      }
      break;
    default:        // only TWO decimals
      IntegerResult = (uint32_t)(ConvertFloat * 100.0f);
      NbrChar = ConvertUint32ToDecimalCharAsciiArray(&ScratchArray[0], sizeof(ScratchArray), IntegerResult);
      CommaPosition = NbrChar - 2;
      for (k = 0; k < CommaPosition; k++) *(DestArray++) = ScratchArray[k];
      *(DestArray++) = '.';
      for (k = 0; k < 2; k++) *(DestArray++) = ScratchArray[CommaPosition + k];
      *DestArray = Null;
      NbrAsciiCharOfString += (NbrChar + 1);
      break;
  }
  return NbrAsciiCharOfString;
}
/****************************************************************************************************/
/* Function to convert an uint32_t (hexadecimal form) into a decimal ASCII representation to        */
/* replace sprintf for long integer. sprintf do not function with long integer.                     */
/* The conversion is made from low significant digit to high significant digit and the number of    */
/* significant digits is returned. The conversion begin with the discover of the unit digit and she */
/* ends by the most significant digit of the decimal representation.                                */
/* principle : modulo by 0x0A (first operation) => result have to be removed from initial value.    */
/* modulo =>          integer value strictly inferior at 0x0A, either the result between 0 and 9.   */
/* soustraction =>    we reduce the value by the result obtained by the modulo function.            */
/*                    (significative digit).                                                        */
/* division =>        division by 0x0A, to fix the new result from which we will applied the modulo */
/*                    function.                                                                     */
/* EXAMPLE: value 0x0358 to convert                                                                 */
/*                    0x358 % A = 6 => 0x358 - 6 = 0x352 => 0x352 / A = 0x55                        */
/*                    0x55 % A = 5 => 0x55 - 5 = 0x50 => 0x50 / A = 0x8                             */
/*                    Result => 0x358 => '8' '5' '6'                                                */
/* The search is ended when the reuslt of the division by A give only one digit strictly inferior   */
/* than 0x0A.                                                                                       */
/* The array which receive ASCII characters with a decimal representation has a dimension of 20     */
/* characters. This array have to be public to be shared by all equivalent functions of this module.*/
/* ConvUintxx_tToAsciiChar[10];     // 2^32 = 4 294 967 296                                         */
/* ConvUintxx_tToAsciiChar[20];     // 2^64 = 18 446 744 073 709 551 616                            */
/* The ASCII string returned in the char array is left aligned.                                     */
/* The array dimension must be at least over 10 cells to represent uint32_t integers.               */
/****************************************************************************************************/
uint8_t ConvertUint32ToDecimalCharAsciiArray(char *ptrTAB, uint8_t ArrayLength, uint32_t valToConvert) {    // 10 possible characters from 0 to 4,294,967,295 (2^32)
  char *ptrINIT;
  uint8_t k;
  uint8_t index;
  uint8_t result_modulo;
  uint32_t result_division;
  ptrINIT = ptrTAB;
  for (k = 0; k < ArrayLength; k++) *(ptrTAB++) = Null;                     // to initialize the array ConvUintxx_tToAsciiChar[20]
  index = ArrayLength - 1;                                                  // the low significant digit in the char array (unity) from 0 to dimension - 1 for index
  ptrTAB = ptrINIT;
  ptrTAB += index * sizeof(char);                                           // to fix the low digit in the char array
  do {
    result_modulo = (uint8_t)(valToConvert % 0x0A);
    *(ptrTAB--) = (char)(result_modulo + 0x30);                             // ASCII char to display
    index--;
    result_division = (valToConvert - result_modulo) / 0x0A;
    valToConvert = result_division;                                         // new value for which we have to define the modulo
  } while (result_division > 9);                                            // if result is between 0 and 9, the loop stops and we can identify all characters to be displayed
  *ptrTAB = (char)(0x30 + result_division);
  ptrTAB = ptrINIT;                                                         // first position in the array
  ptrINIT += index * sizeof(char);                                          // to retrieve the last position of the most significant digit
  for (k = 0; k < ArrayLength - index; k++) *(ptrTAB++) = *(ptrINIT++);     // to retrieve an array starting with the fisrt position [0]
  *ptrTAB = Null;
  return ArrayLength - index;
}
/****************************************************************************************************/
/* Function to get an answer from operator using terminal. This function waits a mandatory answer   */
/* from the terminal. The short answer is 'Y' (Yes) or 'N' (No) but this function also would be     */
/* useful to retrieve other long response.                                                          */
/* common char array is MyAnswer[20].                                                               */
/****************************************************************************************************/
char *FillMyAnswerArray() {
  uint8_t k = 0;
  int InComingByte = Null;
  memset(MyAnswer, Null, sizeof(MyAnswer));       // char MyAnswer[64];
  do {                                            // polling loop
    if (Serial.available() != 0) {
      InComingByte = Serial.read();               // virtual int read(void); from HardwareSerial.h due to larger datas from UART buffer
      MyAnswer[k++] = (char)InComingByte;
      if (InComingByte == '\n' || InComingByte == '\r') break;  // We wait the answer of the operator Yes or No
    }
  } while (1);
  MyAnswer[k - 1] = Null;                         // to replace '\n' or '\r'
  Serial.print(F("[System] Your answer is: "));
  Serial.println(MyAnswer);                       // global array shared by all functions of this module
  return &MyAnswer[0];
}
/****************************************************************************************************/
/* Function to calibrate humidity probe using the terminal to converse between electronic and       */
/* operator.                                                                                        */
/****************************************************************************************************/
void CalibrateHumidityProbe() {
  char *LclPtr;
  char OnlyOneChar;
  boolean CheckAnswer = false;
  uint8_t k;
  uint8_t NbrCharOfString;
  uint8_t ProbeNumber;

  DividerTextForFunctions(80, '_');
  Serial.println(F("[Advice] \uFFED For probe calibration, values will be stored in EEPROM."));
  Serial.println(F("[Query] Do you need to calibrate humidity probes? Y/N (y/n):"));
  LclPtr = FillMyAnswerArray();
  if (*LclPtr == 'Y' || *LclPtr == 'y') {
    Serial.println(F("[Advice] \uFFED You have to calibrate one of the three probes 1, 3 or 4"));
    Serial.println(F("[Query] What number probe do you want to calibrate? (1, 3 or 4):"));
    LclPtr = FillMyAnswerArray();  
    if (*LclPtr == '1' || *LclPtr == '3' || *LclPtr == '4') {
      ProbeNumber = *LclPtr - 0x30;
      Serial.print(F("[Advice] \uFFED First you need to dig up the probe"));
      Serial.print(ProbeNumber, DEC);
      Serial.println(F(" and dry it."));
      Serial.println(F("[Query] Is the probe"));
      Serial.print(ProbeNumber, DEC);
      Serial.println(F(" in the air and is it dry? Y/N (y/n):"));
      LclPtr = FillMyAnswerArray();
      if (*LclPtr == 'N' || *LclPtr == 'n') {
        Serial.println(F("[Advice] \uFFED I am waiting your availability for as long as it takes..."));
        Serial.println(F("[Query] Is the probe"));
        Serial.print(ProbeNumber, DEC);
        Serial.println(F(" in the air and is it dry? Y/N (y/n):"));
        do {
          LclPtr = FillMyAnswerArray();
        } while (*LclPtr != 'Y' || *LclPtr != 'y');
      }
      switch (ProbeNumber) {
        case 1:
          HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);
          Serial.println(F("[Measure] Values for dry probe1"));
          HumMeas1.Potential = DisplayADCcounting_potential(HumMeas1.DigitalFromADC);
          CountingForDryProbe1 = HumMeas1.DigitalFromADC;
          break;
        case 3:
          HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);
          Serial.println(F("[Measure] Values for dry probe3"));
          HumMeas3.Potential = DisplayADCcounting_potential(HumMeas3.DigitalFromADC);
          CountingForDryProbe3 = HumMeas3.DigitalFromADC;
          break;
        case 4:
          HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);
          Serial.println(F("[Measure] Values for dry probe4"));
          HumMeas4.Potential = DisplayADCcounting_potential(HumMeas4.DigitalFromADC);
          CountingForDryProbe4 = HumMeas4.DigitalFromADC;
          break;
      }
      Serial.print(F("\n[Advice] \uFFED Second you need to dive probe"));
      Serial.print(ProbeNumber, DEC);
      Serial.println(F(" entirely into a water glass."));
      Serial.println(F("[Advice] \uFFED It is better to wait several minutes before answering yes."));
      Serial.print(F("[Query] Is the probe"));
      Serial.print(ProbeNumber, DEC);
      Serial.println(F(" immersed in water? Y/N (y/n):"));
      LclPtr = FillMyAnswerArray();
      if (LclPtr[0] == 'N' || LclPtr[0] == 'n') {
        Serial.println(F("[Advice] \uFFED I am waiting your availability for as long as it takes..."));
        Serial.print(F("[Query] Is the probe"));
        Serial.print(ProbeNumber, DEC);
        Serial.println(F(" immersed in water? Y/N (y/n):"));
        do {
          LclPtr = FillMyAnswerArray();
          if (*LclPtr == 'Y' || *LclPtr == 'y') CheckAnswer = true;
          else CheckAnswer = false;
          //Serial.print(F("Character: "));
          //Serial.println(*LclPtr);
        } while (CheckAnswer == false);       //(LclPtr[0] != 'Y' || LclPtr[0] != 'y');
      }
      switch (ProbeNumber) {
        case 1:
          HumMeas1.DigitalFromADC = MyADC_Probe1.readADC(Channel_ADC);
          Serial.println(F("[Measure] Values for wet probe1"));
          HumMeas1.Potential = DisplayADCcounting_potential(HumMeas1.DigitalFromADC);
          CoeffProbe1.Slope = 90.0f / ((float)HumMeas1.DigitalFromADC - (float)CountingForDryProbe1);   // a' (float)
          Serial.print(F("\n\uFFED New slope for Probe1 has been calculated: "));
          memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
          NbrCharOfString = ConvertFloatToStringWithSign(CoeffProbe1.Slope, 7, &ArrayForNewSlope[0]);
          LclPtr = &ArrayForNewSlope[0];
          for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
          Serial.println();
          CoeffProbe1.y_intercept_corrected = (float)(90.0f - (float)(HumMeas1.DigitalFromADC * CoeffProbe1.Slope));   // second correction with the new slope
          Serial.print(F("\uFFED New y intercept for Probe1 has been calculated: "));
          memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
          NbrCharOfString = ConvertFloatToStringWithSign(CoeffProbe1.y_intercept_corrected, 4, &ArrayForNewYintercept[0]);
          LclPtr = &ArrayForNewYintercept[0];
          for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
          Serial.println();
          EEPROM.put(AddEepromProbe1, CoeffProbe1);
          Serial.println(F("\u2714 Slope and Y-intercept for probe1 have been stored in EEPROM"));
          break;
        case 3:
          HumMeas3.DigitalFromADC = MyADC_Probe3.readADC(Channel_ADC);
          Serial.println(F("[Measure] Values for wet probe3"));
          HumMeas3.Potential = DisplayADCcounting_potential(HumMeas3.DigitalFromADC);
          CoeffProbe3.Slope = 90.0f / ((float)HumMeas3.DigitalFromADC - (float)CountingForDryProbe3);    // a' (float)
          Serial.print(F("\n\uFFED New slope for Probe3 has been calculated: "));
          memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
          NbrCharOfString = ConvertFloatToStringWithSign(CoeffProbe3.Slope, 7, &ArrayForNewSlope[0]);
          LclPtr = &ArrayForNewSlope[0];
          for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
          Serial.println();
          CoeffProbe3.y_intercept_corrected = (float)(90.0f - (float)(HumMeas3.DigitalFromADC * CoeffProbe3.Slope));   // second correction with the new slope
          Serial.print(F("\uFFED New y intercept for Probe3 has been calculated: "));
          memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
          NbrCharOfString = ConvertFloatToStringWithSign(CoeffProbe3.y_intercept_corrected, 4, &ArrayForNewYintercept[0]);
          LclPtr = &ArrayForNewYintercept[0];
          for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
          Serial.println();
          EEPROM.put(AddEepromProbe3, CoeffProbe3);
          Serial.println(F("\u2714 Slope and Y-intercept for probe3 have been stored in EEPROM"));
          break;
        case 4:
          HumMeas4.DigitalFromADC = MyADC_Probe4.readADC(Channel_ADC);
          Serial.println(F("[Measure] Values for wet probe4"));
          HumMeas4.Potential = DisplayADCcounting_potential(HumMeas4.DigitalFromADC);
          CoeffProbe4.Slope = 90.0f / ((float)HumMeas4.DigitalFromADC - (float)CountingForDryProbe4);    // a' (float)
          Serial.print(F("\n\uFFED New slope for Probe4 has been calculated: "));
          memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
          NbrCharOfString = ConvertFloatToStringWithSign(CoeffProbe4.Slope, 7, &ArrayForNewSlope[0]);
          LclPtr = &ArrayForNewSlope[0];
          for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
          Serial.println();
          CoeffProbe4.y_intercept_corrected = (float)(90.0f - (float)(HumMeas4.DigitalFromADC * CoeffProbe4.Slope));   // second correction with the new slope
          Serial.print(F("\uFFED New y intercept for Probe4 has been calculated: "));
          memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
          NbrCharOfString = ConvertFloatToStringWithSign(CoeffProbe4.y_intercept_corrected, 4, &ArrayForNewYintercept[0]);
          LclPtr = &ArrayForNewYintercept[0];
          for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
          Serial.println();
          EEPROM.put(AddEepromProbe4, CoeffProbe4);
          Serial.println(F("\u2714 Slope and Y-intercept for probe4 have been stored in EEPROM"));
          break;
      }
    }
  }
  DividerTextForFunctions(80, '_');
}
/****************************************************************************************************/
/* Text divider                                                                                     */
/****************************************************************************************************/
void DividerTextForFunctions(uint8_t nbr_carac, char caract) {
  uint8_t i;
  for (i = 0; i < nbr_carac; i++) Serial.print(caract);
  Serial.println();
}
/****************************************************************************************************/
/* Function to display only potentiel measured and digital output from ADC.                         */
/****************************************************************************************************/
float DisplayADCcounting_potential(uint16_t counting) {
  float Potential;
  Serial.print(F("\u2714 Counting from ADC: "));
  Serial.println(counting, DEC);
  Potential = ((float)counting * gain2_048) / (float)ADS1115FullScale;
  Serial.print(F("\u2714 Voltage measured from ADS1115: "));
  Serial.print(Potential);
  if (Potential >= 2.0f) Serial.println(F(" volts"));
  else Serial.println(F(" volt"));
  return Potential;
}
/****************************************************************************************************/
/* Dispaly content of the memory EEPROM.                                                            */
/****************************************************************************************************/
void DisplayEepromContents() {
  DividerTextForFunctions(80, '#');
  EEPROM.get(AddEepromProbe1, CoeffProbe1);
  Serial.print(F("\u2714 EEPROM at address 0x"));
  Serial.println(AddEepromProbe1, HEX);
  Serial.print(F("\t\tY_intercept: "));
  Serial.println(CoeffProbe1.y_intercept_corrected, 4);
  Serial.print(F("\t\tSlope: "));
  Serial.println(CoeffProbe1.Slope, 7);
  EEPROM.get(AddEepromProbe3, CoeffProbe3);
  Serial.print(F("\u2714 EEPROM at address 0x"));
  Serial.println(AddEepromProbe3, HEX);
  Serial.print(F("\t\tY_intercept: "));
  Serial.println(CoeffProbe3.y_intercept_corrected, 4);
  Serial.print(F("\t\tSlope: "));
  Serial.println(CoeffProbe3.Slope, 7);
  EEPROM.get(AddEepromProbe4, CoeffProbe1);
  Serial.print(F("\u2714 EEPROM at address 0x"));
  Serial.println(AddEepromProbe4, HEX);
  Serial.print(F("\t\tY_intercept: "));
  Serial.println(CoeffProbe4.y_intercept_corrected, 4);
  Serial.print(F("\t\tSlope: "));
  Serial.println(CoeffProbe4.Slope, 7);
  DividerTextForFunctions(80, '#');
}
/****************************************************************************************************/
/* Dispaly content of a calculus to finf Slope parameter.                                           */
/****************************************************************************************************/
void DisplayCalculusForSlope() {
  float lclSlope;
  float lclY_intercept;
  uint8_t k;
  char *LclPtr;
  uint8_t NbrCharOfString;
  uint16_t Count1 = 22717;
  uint16_t Count2 = 9230;
  lclSlope = 90.0f / ((float)Count2 - (float)Count1);
  //if (lclSlope < 0.00f) Serial.println(F("Float number negative"));
  Serial.print(F("\n\uFFED Slope calculated: "));
  memset(ArrayForNewSlope, Null, sizeof(ArrayForNewSlope));
  NbrCharOfString = ConvertFloatToStringWithSign(lclSlope, 7, &ArrayForNewSlope[0]);
  LclPtr = &ArrayForNewSlope[0];
  for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
  Serial.println();
  lclY_intercept = (float)(90.0f - 9230 * lclSlope);              // second correction with the new slope
  Serial.print(F("\uFFED Y intercept calculated: "));
  memset(ArrayForNewYintercept, Null, sizeof(ArrayForNewYintercept));
  NbrCharOfString = ConvertFloatToStringWithSign(lclY_intercept, 4, &ArrayForNewYintercept[0]);
  LclPtr = &ArrayForNewYintercept[0];
  for (k = 0; k < NbrCharOfString; k++) Serial.print(*(LclPtr++));
  Serial.println();
}












/* ######################################################################################################## */
// END of file
